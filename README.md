# rbbs

RBBS (Request-based Bulletin Board System)
is a simple bulletin board, written with Artanis, which can be accessed via terminal emulator using simple tools, and operates on a per-request basis (the web-server model) rather than active login sessions.

TODO List:
* Add posting timeouts to prevent spam
